use gtk::{
    EditableSignals,
    EntryExt,
    Inhibit,
    LabelExt,
    OrientableExt,
    WidgetExt,
    ButtonExt,
};

use gtk::Orientation::{Vertical, Horizontal};
use relm::Widget;
use relm_derive::{Msg, widget};

use self::Msg::*;

pub struct Model {
    a1: f32,
    b1: f32,
    c1: f32,
    r1: String,

    a2: f32,
    b2: f32,
    c2: f32,
    r2: String,

    result: String,
    type_p: u8,
    koof: f32,
    text: String,

    v2vis: bool,
}


#[derive(Msg)]
pub enum Msg {
    Change(String, u8),
    ClearAll,
    Switch,
    Quit,
}

#[widget]
impl Widget for Win {
    fn model() -> Model {
        Model {
            a1: -1_f32,
            b1: -1_f32,
            c1: -1_f32,
            r1: " ".to_string(),

            a2: -1_f32,            
            b2: -1_f32,
            c2: -1_f32,
            r2: " ".to_string(),
            result: " ".to_string(),
            type_p: 0,
            koof: 0.75,
            
            text: "Объём мочевого пузыря".to_string(),

            v2vis: false,
        }
    }

    fn update(&mut self, event: Msg) {
        match event {
            Change(text, number) => {
                match &number {
                    0 => self.model.a1 = text.parse()
                                            .unwrap_or(-1_f32),

                    1 => self.model.b1 = text.parse()
                                            .unwrap_or(-1_f32),

                    2 => self.model.c1 = text.parse()
                                            .unwrap_or(-1_f32),
                   
                    3 => self.model.a2 = text.parse()
                                            .unwrap_or(-1_f32),
                    
                    4 => self.model.b2 = text.parse()
                                            .unwrap_or(-1_f32),
                    
                    5 => self.model.c2 = text.parse()
                                            .unwrap_or(-1_f32),
                    
                    _ => print!("Error 2")
                    

                };

                if (0..3).contains(&number) {
                    self.match_all(0);
                }

                else {
                    self.match_all(1);
                };
                self.match_all(3);
            },

            ClearAll => {
                self.entry_a1.set_text("");
                self.entry_b1.set_text("");
                self.entry_c1.set_text("");
                self.entry_a2.set_text("");
                self.entry_b2.set_text("");
                self.entry_c2.set_text("");
            },

            Switch => {
                match self.model.type_p {
                    0 => {
                        self.model.type_p = 1;
                        self.model.koof = 0.523;
                        self.model.text = "Объём предстательной железы".to_string();
                        self.model.v2vis = false;
                    },
                    1 => {
                        self.model.type_p = 2;
                        self.model.koof = 0.48;
                        self.model.text = "Объём щитовидной железы".to_string();
                        self.model.v2vis = true;
                    },
                    2 => {
                        self.model.type_p = 0;
                        self.model.koof = 0.75;
                        self.model.text = "Объём мочевого пузыря".to_string();
                        self.model.v2vis = false;
                    },
                    _ => {
                        println!("Error 1");
                    }
                }
                self.clear_all();
            }

            Quit => gtk::main_quit(),
            
        }
    }
    fn clear_all(&mut self) {
        self.entry_a1.set_text("");
        self.entry_b1.set_text("");
        self.entry_c1.set_text("");
        self.entry_a2.set_text("");
        self.entry_b2.set_text("");
        self.entry_c2.set_text("");
    }
    fn match_all(&mut self, fo1: u8){
        match fo1 {
            0 =>if (self.model.a1 != -1.0_f32) &
                   (self.model.b1 != -1.0_f32) &
                   (self.model.c1 != -1.0_f32) 
                {
                    self.model.r1 = format!("V1 = {} cm³.", self.model.a1 * 
                                                           self.model.b1 * 
                                                           self.model.c1 * self.model.koof);
                }
                else {
                    self.model.r1 = " ".to_string();               
                },

            1 => {
                    if self.model.v2vis & 
                        (self.model.a2 != -1.0_f32) &
                        (self.model.b2 != -1.0_f32) &
                        (self.model.c2 != -1.0_f32) 
                    {
                        self.model.r2 = format!("V2 = {} cm³.", self.model.a2 *
                                                               self.model.b2 * 
                                                               self.model.c2 * self.model.koof);
                    }
                    else {
                        self.model.r2 = " ".to_string();               
                    }
            },         

            2 => {
                    self.model.r1 = format!("{}", self.model.a1 * 
                                                  self.model.b1 * 
                                                  self.model.c1 *  self.model.koof);

                    self.model.r2 = format!("{}", self.model.a2 *
                                                  self.model.b2 *
                                                  self.model.c2 *  self.model.koof);
            },

            3 => {
                    if  self.model.v2vis & 
                        (self.model.a1 != -1.0_f32) &
                        (self.model.b1 != -1.0_f32) &
                        (self.model.c1 != -1.0_f32) &
                        (self.model.a2 != -1.0_f32) &
                        (self.model.b2 != -1.0_f32) &
                        (self.model.c2 != -1.0_f32) {
                        self.model.result = format! ("Общий объём = {} cm³.", 
                                                    self.model.a1 * 
                                                    self.model.b1 * 
                                                    self.model.c1 * 
                                                        self.model.koof +
                                                    self.model.a2 * 
                                                    self.model.b2 * 
                                                    self.model.c2 * 
                                                        self.model.koof);
                    }
                    else if !self.model.v2vis &
                        (self.model.a1 != -1.0_f32) &
                        (self.model.b1 != -1.0_f32) &
                        (self.model.c1 != -1.0_f32) {
                        self.model.result = format! ("Общий объём = {} cm³.",
                                                    self.model.a1 *
                                                    self.model.b1 *
                                                    self.model.c1 * 
                                                        self.model.koof);
                    }

                    else {
                        self.model.result = " ".to_string();               
                    };
            },

            _ => print!("")
        }
    }

    view! {
        gtk::Window {
          gtk::Box{
            orientation: Vertical,
            
            gtk::Label {
                text: &self.model.text
            },

            gtk::Label {
                text: "v1" 
            },

            gtk::Box {
                orientation: Horizontal,
                #[name="entry_a1"]
                gtk::Entry {
                    changed(entry) => {
                        let text = entry.get_text()
                                        .expect("get_text failed")
                                        .to_string();

                        let len = text.len();
                        Change(text, 0)
                    },
                    width_chars: 10,
                    placeholder_text: Some("Длина cm"),
                },
               
                #[name="entry_b1"]
                gtk::Entry {
                    changed(entry) => {
                        let text = entry.get_text()
                                        .expect("get_text failed")                                        
                                        .to_string();

                        //println!("{}",text);
                        Change(text, 1)

                    },
                    width_chars: 10,
                    placeholder_text: Some("Ширина cm"),
                },

                #[name="entry_c1"]
                gtk::Entry {
                    changed(entry) => {
                        let text = entry.get_text()
                                        .expect("get_text failed")                                        
                                        .to_string();

                        //println!("{}",text);
                        Change(text, 2)

                    },
                    width_chars: 10,
                    placeholder_text: Some("Высота cm"),
                },
                
                #[name="label1"]
                gtk::Label {
                    text: self.model.r1.as_str(),
                    width_chars: 10,
                },

            },

            gtk::Label {
                text: "v2", 
                visible: self.model.v2vis,
            },
            #[name="inbox2"]
            gtk::Box {
                visible: self.model.v2vis,
                orientation: Horizontal,

                #[name="entry_a2"]
                gtk::Entry {
                    changed(entry) => {
                        let text = entry.get_text()
                                        .expect("get_text failed")
                                        .to_string();

                        let len = text.len();
                        Change(text, 3)
                    },
                    placeholder_text: Some("Длина cm"),
                    width_chars: 10,
                },

                #[name="entry_b2"]
                gtk::Entry {
                    changed(entry) => {
                        let text = entry.get_text()
                                        .expect("get_text failed")
                                        .to_string();

                        let len = text.len();
                        Change(text, 4)
                    },
                    placeholder_text: Some("Ширина cm"),
                    width_chars: 10,
                },
                

                #[name="entry_c2"]
                gtk::Entry {
                    changed(entry) => {
                        let text = entry.get_text()
                                        .expect("get_text failed")
                                        .to_string();

                        //println!("{}",text);
                        Change(text, 5)

                    },
                    placeholder_text: Some("Высота cm"),
                    width_chars: 10,
                },
                
                #[name="label2"]
                gtk::Label {
                    text: &self.model.r2.as_str(),
                    width_chars: 10,
                },
            },
            gtk::Box {
                orientation: Horizontal,
                
                #[name="label_f"]
                gtk::Label {
                    text: &self.model.result.as_str(),
                },

            },

            gtk::Box {
                gtk::Button {
                    clicked => {
                       ClearAll
                    },
                    label:"Очистить",
                },
                
                gtk::Button{
                    clicked => {
                        Switch
                    },
                    label: &self.model.text,
                },
            }
          },
          delete_event(_, _) => (Quit, Inhibit(false)),
        },
        
    }
}


fn main() {
    Win::run(()).expect("Win::run failed");
}

#[cfg(test)]
mod tests {
    use gdk::enums::key;
    use gtk::LabelExt;

    use gtk_test::{assert_text, enter_key, enter_keys};

    use crate::Win;

    #[test]
    fn label_change() {
        let (_component, widgets) = relm::init_test::<Win>(()).expect("init_test failed");
        let entry = &widgets.entry;
        let label = &widgets.label;

        assert_text!(label, "");

        enter_keys(entry, "test");
        assert_text!(label, "tset (4)");

        enter_key(entry, key::BackSpace);
        assert_text!(label, "set (3)");

        enter_key(entry, key::Home);
        //enter_key(entry, key::Delete); // TODO: when supported by enigo.
        enter_keys(entry, "a");
        assert_text!(label, "seta (4)");

        enter_key(entry, key::End);
        enter_keys(entry, "a");
        //assert_text!(label, "aseta (5)"); // FIXME
    }
}
